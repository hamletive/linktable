package menu

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func New() *Processor {
	return &Processor{make(map[string]func([]string))}
}

type Processor struct {
	handlers map[string]func([]string)
}

func (p *Processor) Register(cmd string, handler func([]string)) {
	if _, ok := p.handlers[cmd]; !ok {
		p.handlers[cmd] = handler
	} else {
		panic("command already registered")
	}
}

func (p *Processor) Run() {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Printf("> ")
		ok := scanner.Scan()
		if !ok {
			break
		}
		cmdline := scanner.Text()
		parts := strings.Fields(cmdline)
		if len(parts) == 0 {
			fmt.Printf("no command specified")
			continue
		}
		handler, ok := p.handlers[parts[0]]
		if ok {
			handler(parts[1:])
		} else {
			fmt.Printf("no such command")
			continue
		}
	}
}
