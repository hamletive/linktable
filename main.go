package main

import (
	"fmt"
	"linktable/linktable"
	"linktable/menu"
	"math/rand"
	"strconv"
)

func main() {
	p := menu.New()
	l := linktable.New[int]()
	p.Register("prepend", func(args []string) {
		n, err := strconv.Atoi(args[0])
		if err != nil {
			panic(err)
		}
		l.Prepend(n)
	})
	p.Register("append", func(args []string) {
		n, err := strconv.Atoi(args[0])
		if err != nil {
			panic(err)
		}
		l.Append(n)
	})
	p.Register("show", func(args []string) {
		fmt.Printf("Linked Table: %+v\n", l.ToSlice())
	})
	p.Register("popleft", func(args []string) {
		fmt.Printf("Popped %v\n", l.RemoveHead())
	})
	p.Register("popright", func(args []string) {
		fmt.Printf("Popped %v\n", l.RemoveTail())
	})
	p.Register("find", func(args []string) {
		n, err := strconv.Atoi(args[0])
		if err != nil {
			panic(err)
		}
		fmt.Printf("Index: %v\n", l.Find(n))
	})
	p.Run()
}

func main2() {
	l := linktable.New[int]()
	for i := 1; i <= 10000; i++ {
		if rand.Intn(2) == 0 {
			if l.Len() != 0 {
				if rand.Intn(2) == 0 {
					l.RemoveHead()
				} else {
					l.RemoveTail()
				}
			}
		} else {
			if rand.Intn(2) == 0 {
				l.Prepend(i)
			} else {
				l.Append(i)
			}
		}
		fmt.Printf("%+v\n", l.ToSlice())
	}
}
